<?php

declare(strict_types=1);
/**
 * @copyright Copyright (c) 2019 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\Tests\Quota;

use OCA\InstanceQuota\Quota\QuotaManager;
use OCP\Files\FileInfo;
use OCP\IConfig;
use OCP\IUser;
use Test\TestCase;

class QuotaManagerTest extends TestCase {

	/**
	 * @dataProvider dataForTestGetQuota
	 */
	public function testGetQuota(?int $quota) {
		$config = $this->createMock(IConfig::class);
		$config->expects($this->once())->method('getAppValue')->with('instance_quota', 'quota', (string)FileInfo::SPACE_UNLIMITED)->willReturn($quota ?? FileInfo::SPACE_UNLIMITED);
		$quotaManager = new QuotaManager($config);

		$this->assertEquals($quota ?? FileInfo::SPACE_UNLIMITED, $quotaManager->getQuota());
	}

	public function dataForTestGetQuota(): array {
		return [
			[1024],
			[null]
		];
	}

	/**
	 * @dataProvider dataForTestGetUserQuota
	 */
	public function testGetUserQuota(?int $quota, string $userQuota, bool $useInstanceQuota, int $result) {
		$user = $this->createMock(IUser::class);
		$user->expects($this->once())->method('getQuota')->willReturn($userQuota);
		$config = $this->createMock(IConfig::class);
		if ($useInstanceQuota) {
			$config->expects($this->once())->method('getAppValue')->with('instance_quota', 'quota', (string)FileInfo::SPACE_UNLIMITED)->willReturn($quota ?? FileInfo::SPACE_UNLIMITED);
		}
		$quotaManager = new QuotaManager($config);
		$this->assertEquals($result, $quotaManager->getUserQuota($user));
	}

	public function dataForTestGetUserQuota(): array {
		return [
			[1024, 'none', true, 1024],
			[null, 'none', true, FileInfo::SPACE_UNLIMITED],
			[null, 'bad quota', true, FileInfo::SPACE_UNLIMITED],
			[1024, 'bad quota', true, 1024],
			[1024, '1GB', false, 1073741824],
			[null, '1GB', false, 1073741824]
		];
	}

	public function testSetUserQuota() {
		$config = $this->createMock(IConfig::class);
		$config->expects($this->once())->method('setAppValue')->with('instance_quota', 'quota', 1024);
		$quotaManager = new QuotaManager($config);
		$quotaManager->setQuota(1024);
	}
}
