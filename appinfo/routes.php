<?php

return [
	'ocs' => [
		['name' => 'QuotaAPI#setQuota', 'url' => '/api/v1/quota', 'verb' => 'POST'],
		['name' => 'QuotaAPI#getQuota', 'url' => '/api/v1/quota', 'verb' => 'GET'],
	],
];
