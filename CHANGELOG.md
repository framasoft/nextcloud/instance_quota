# Changelog

## 0.1.1 (2024-02-09)

### Bug Fixes

* various fixes on how quota calculation is done ([df7454e](https://framagit.org/framasoft/nextcloud/instance_quota/-/commit/df7454e))

## 0.1.0 (2024-02-05)

* feat: add CHANGELOG ([487c8fc](https://framagit.org/framasoft/nextcloud/instance_quota/-/commit/487c8fc))
* feat: initial release ([62258b7](https://framagit.org/framasoft/nextcloud/instance_quota/-/commit/62258b7))
* ci: add CI file ([dca528c](https://framagit.org/framasoft/nextcloud/instance_quota/-/commit/dca528c))
* fix(readme): change OCS example to XML and correct URL ([192d7cf](https://framagit.org/framasoft/nextcloud/instance_quota/-/commit/192d7cf))
* fix(readme): fix urls ([57e954e](https://framagit.org/framasoft/nextcloud/instance_quota/-/commit/57e954e))
