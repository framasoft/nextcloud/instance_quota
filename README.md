# Instance Quota

Allow setting a disk quota for the whole server.

## Usage

Instance quota can only be configured through the command line or OCS api, no admin UI is currently available.

### OCC commandline API

#### Get the quota

```bash
occ instancequota:get
```

#### Get the used space

```bash
occ instancequota:used
```

#### Set the quota

```bash
occ instancequota:set 2GB
```

All commands accept a `--format`(`-f`) option to format their output in a human-readable format.

### OCS Rest API

#### Get the quota and used space

```bash
curl -u admin:admin -H 'OCS-APIRequest: true' -H 'Accept: application/json' https://example.com/ocs/v2.php/apps/instance_quota/api/v1/quota
```

```json
{
	"ocs": {
		"meta": {
			"status": "ok",
			"statuscode": 200,
			"message": "OK"
		},
		"data": {
			"quota_bytes": 10485760,
			"quota_human": "10 MB",
			"used_bytes": 818050,
			"used_human": "799 KB",
			"used_relative": 7.8
		}
	}
}
```

#### Set the quota

Set the quota of to 2GB

```bash
curl -u admin:admin -H 'OCS-APIRequest: true' -H 'Accept: application/json' https://example.com/ocs/v2.php/apps/instance_quota/api/v1/quota -X POST -d 'quota=2GB'
```

The new quota information will also be returned in the same format as from a `GET` request.

## Limitations

This app is not compatible with the [GroupQuota app](https://github.com/nextcloud/groupquota).
