<?php
/**
 * @copyright Copyright (c) 2018 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\Quota;

use OCP\DB\QueryBuilder\IQueryBuilder;
use OCP\IDBConnection;

class UsedSpaceCalculator {
	private IDBConnection $connection;

	public function __construct(IDBConnection $connection) {
		$this->connection = $connection;
	}

	public function getGlobalUsedSpace(): int {
		$query = $this->connection->getQueryBuilder();

		$query->select($query->func()->sum('size'))
			->from('filecache', 'f')
			->innerJoin('f', 'mounts', 'm', $query->expr()->andX(
				$query->expr()->eq('storage_id', 'storage'),
				$query->expr()->eq('path_hash', $query->createNamedParameter(md5('files')))
			))
			->andWhere($query->expr()->gte('size', $query->expr()->literal(0, IQueryBuilder::PARAM_INT)));

		$result = $query->executeQuery()->fetchOne();
		if ($result === false) {
			return 0;
		}
		return (int) $result;
	}
}
