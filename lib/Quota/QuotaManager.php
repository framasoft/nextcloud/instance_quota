<?php
/**
 * @copyright Copyright (c) 2018 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\Quota;

use OCP\Files\FileInfo;
use OCP\IConfig;
use OCP\IUser;
use OCP\Util;

class QuotaManager {
	public function __construct(private IConfig $config) {
	}

	public function getQuota(): int {
		return (int)$this->config->getAppValue('instance_quota', 'quota', (string)FileInfo::SPACE_UNLIMITED);
	}

	public function getUserQuota(IUser $user): int {
		$userQuota = $user->getQuota();
		if ($userQuota === 'none') {
			return $this->getQuota();
		}
		$userQuota = Util::computerFileSize($userQuota);
		if ($userQuota === false) {
			return $this->getQuota();
		}
		return (int) $userQuota;
	}

	public function setQuota(int $quota): void {
		$this->config->setAppValue('instance_quota', 'quota', (string)$quota);
	}
}
