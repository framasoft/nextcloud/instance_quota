<?php
/**
 * @copyright Copyright (c) 2018 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\AppInfo;

use OC\Files\Filesystem;
use OC\Files\Storage\Home;
use OCA\InstanceQuota\Quota\QuotaManager;
use OCA\InstanceQuota\Quota\UsedSpaceCalculator;
use OCA\InstanceQuota\Wrapper\InstanceQuotaWrapper;
use OCP\AppFramework\App;
use OCP\AppFramework\Bootstrap\IBootContext;
use OCP\AppFramework\Bootstrap\IBootstrap;
use OCP\AppFramework\Bootstrap\IRegistrationContext;
use OCP\Files\FileInfo;
use OCP\Files\IHomeStorage;
use OCP\Files\Storage\IStorage;
use OCP\Util;

class Application extends App implements IBootstrap {
	public function __construct(array $urlParams = []) {
		parent::__construct('instance_quota', $urlParams);
	}

	public function register(IRegistrationContext $context): void {
	}


	public function boot(IBootContext $context): void {
		Util::connectHook('OC_Filesystem', 'preSetup', $this, 'addStorageWrapper');
	}

	private function getUsedSpaceCalculator(): UsedSpaceCalculator {
		return $this->getContainer()->get(UsedSpaceCalculator::class);
	}

	private function getQuotaManager(): QuotaManager {
		return $this->getContainer()->get(QuotaManager::class);
	}

	public function addStorageWrapper(): void {
		Filesystem::addStorageWrapper('groupquota', function (string $mountPoint, IStorage $storage) {
			if ($storage->instanceOfStorage(IHomeStorage::class)) {
				/** @var Home $storage */
				$user = $storage->getUser();
				$userQuota = $this->getQuotaManager()->getUserQuota($user);
				$globalQuota = $this->getQuotaManager()->getQuota();
				$globalUsedSpace = $this->getUsedSpaceCalculator()->getGlobalUsedSpace();
				$userUsedSpace = $storage->getCache()->get('files')['size'];
				$globalAvailableSpace = $globalQuota - $globalUsedSpace;

				$rootSize = $globalUsedSpace;
				// If the user has it's own quota
				if ($userQuota !== $globalQuota) {
					// We use it's own root size
					$rootSize = $userUsedSpace;
					// If there's not any available space left to match the quota
					// We reduce the quota
					$userQuota = min($globalAvailableSpace, $userQuota);
				}

				if ($userQuota !== FileInfo::SPACE_UNLIMITED) {
					return new InstanceQuotaWrapper([
						'storage' => $storage,
						'root_size' => $rootSize,
						'quota' => $userQuota,
						'root' => 'files'
					]);
				}
			}
			return $storage;
		});
	}
}
