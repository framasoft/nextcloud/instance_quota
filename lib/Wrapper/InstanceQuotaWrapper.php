<?php
/**
 * @copyright Copyright (c) 2018 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\Wrapper;

use OC\Files\Storage\Wrapper\Quota;

class InstanceQuotaWrapper extends Quota {
	/** @var int $rootSize */
	private mixed $rootSize;

	public function __construct($parameters) {
		parent::__construct($parameters);
		$this->rootSize = $parameters['root_size'];
	}

	public function getCache($path = '', $storage = null): InstanceUsedSpaceCacheWrapper {
		$parentCache = parent::getCache($path, $storage);
		return new InstanceUsedSpaceCacheWrapper($parentCache, $this->sizeRoot, $this->rootSize);
	}
}
