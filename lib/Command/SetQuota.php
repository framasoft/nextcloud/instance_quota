<?php
/**
 * @copyright Copyright (c) 2019 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\Command;

use OC\Core\Command\Base;
use OCA\InstanceQuota\Quota\QuotaManager;
use OCP\Files\FileInfo;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SetQuota extends Base {

	public function __construct(
		private QuotaManager $quotaManager
	) {
		parent::__construct();
	}

	protected function configure(): void {
		$this
			->setName('instancequota:set')
			->setDescription('Set the configured quota')
			->addArgument('quota', InputArgument::REQUIRED, 'The quota to set')
			->addOption('format', 'f', InputOption::VALUE_NONE, 'Format the quota to be "human readable"');
		parent::configure();
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		$quota = \OC_Helper::computerFileSize((string) $input->getArgument('quota'));
		$this->quotaManager->setQuota($quota);
		if ($input->getOption('format')) {
			$quota = $quota === FileInfo::SPACE_UNLIMITED ? 'Unlimited' : \OC_Helper::humanFileSize($quota);
		}
		$output->writeln((string)$quota);

		return 0;
	}
}
