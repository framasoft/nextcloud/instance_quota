<?php
/**
 * @copyright Copyright (c) 2018 Robin Appelman <robin@icewind.nl>
 *
 * @author Thomas Citharel <nextcloud@tcit.fr>
 *
 * @license GNU AGPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

namespace OCA\InstanceQuota\Controller;

use OC_Helper;
use OCA\InstanceQuota\Quota\QuotaManager;
use OCA\InstanceQuota\Quota\UsedSpaceCalculator;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\OCSController;
use OCP\IRequest;

class QuotaAPIController extends OCSController {

	public function __construct(
		string $AppName,
		IRequest $request,
		private QuotaManager $quotaManager,
		private UsedSpaceCalculator $usedSpaceCalculator,
	) {
		parent::__construct($AppName, $request);
	}

	/**
	 * Setting the quota for the whole instance
	 *
	 * @param string $quota The quota to give for the whole instance
	 *
	 * @return DataResponse<Http::STATUS_OK, array{quota_bytes: float, quota_human: string, used_bytes: float, used_human: string, used_relative: float}, array{}>|DataResponse<Http::STATUS_BAD_REQUEST, array{message: string}, array{}>
	 *
	 * 200: Quota successfully set
	 * 400: Invalid quota given
	 */
	public function setQuota(string $quota): DataResponse {
		$quotaBytes = OC_Helper::computerFileSize($quota);
		if (!$quotaBytes) {
			return new DataResponse([
				'message' => 'Invalid quota'
			], Http::STATUS_BAD_REQUEST);
		}
		$this->quotaManager->setQuota($quotaBytes);
		$used = $this->usedSpaceCalculator->getGlobalUsedSpace();
		return $this->buildQuotaResponse($quotaBytes, $used);
	}

	/**
	 * Getting the quota for the whole instance
	 *
	 * @return DataResponse<Http::STATUS_OK, array{quota_bytes: float, quota_human: string, used_bytes: float, used_human: string, used_relative: float}, array{}>
	 *
	 * 200: Returns the quota successfully
	 */
	public function getQuota(): DataResponse {
		$quotaBytes = $this->quotaManager->getQuota();
		$used = $this->usedSpaceCalculator->getGlobalUsedSpace();
		return $this->buildQuotaResponse($quotaBytes, $used);
	}

	private function buildQuotaResponse(float|int $quotaBytes, float|int $used): DataResponse {
		return new DataResponse([
			'quota_bytes' => $quotaBytes,
			'quota_human' => OC_Helper::humanFileSize($quotaBytes),
			'used_bytes' => $used,
			'used_human' => OC_Helper::humanFileSize($used),
			'used_relative' => round($used / $quotaBytes * 100, 2)
		]);
	}
}
